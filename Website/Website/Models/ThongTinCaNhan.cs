﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class ThongTinCaNhan
    {
        [Key]
        [ForeignKey("UserProfile")]
        public int UserProfileUserId { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        [Required(ErrorMessage = "Nhập Tên!")]
        [StringLength(50, ErrorMessage = "Tối đa thiểu 6 kí tự!", MinimumLength = 6)]
        [Display(Name = "Tên của bạn")]
        public string HoTen { set; get; }
        [Display(Name = "Giới tính")]
        public string GioiTinh { set; get; }

        [Required(ErrorMessage = "Nhập ngày sinh!")]
        [Display(Name = "Ngày sinh")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy")]
        public DateTime NgaySinh { set; get; }

        [Required(ErrorMessage = "Nhập địa chỉ email!")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Địa chỉ email không đúng!")]
        [Display(Name = "Địa chỉ mail")]
        public string Email { set; get; }

        [Required(ErrorMessage = "Nhập số điện thoại!")]
        [StringLength(20, ErrorMessage = "10-20 kí tự!", MinimumLength = 10)]
        [Display(Name = "Số điện thoại")]
        public string SDT { set; get; }
        [Display(Name = "Số bài đăng")]
        public int SoBaiDang { set; get; }
        [Display(Name = "Số lượt đánh giá")]
        public int LuotDanhGia { set; get; }
        [Display(Name = "Bình luận")]
        public int DoTinCay { set; get; }
    }
}