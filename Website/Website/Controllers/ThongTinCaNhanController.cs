﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models;

namespace Website.Controllers
{
    [Authorize]
    public class ThongTinCaNhanController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /ThongTinCaNhan/

        public ActionResult Index()
        {
            int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).Where(y => y.UserName == User.Identity.Name).Single().UserId;
            var thongtincanhans = db.ThongTinCaNhans.Where(t => t.UserProfileUserId==userid);
            return View(thongtincanhans.ToList());
        }

        //
        // GET: /ThongTinCaNhan/Details/5

        public ActionResult Details(int id = 0)
        {
            ThongTinCaNhan thongtincanhan = db.ThongTinCaNhans.Find(id);
            if (thongtincanhan == null)
            {
                return HttpNotFound();
            }
            return View(thongtincanhan);
        }

        //
        // GET: /ThongTinCaNhan/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /ThongTinCaNhan/Create

        [HttpPost]
        public ActionResult Create(ThongTinCaNhan thongtincanhan)
        {
            if (ModelState.IsValid)
            {
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).Where(y => y.UserName == User.Identity.Name).Single().UserId;
                thongtincanhan.UserProfileUserId = userid;
                db.ThongTinCaNhans.Add(thongtincanhan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", thongtincanhan.UserProfileUserId);
            return View(thongtincanhan);
        }

        //
        // GET: /ThongTinCaNhan/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ThongTinCaNhan thongtincanhan = db.ThongTinCaNhans.Find(id);
            if (thongtincanhan == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", thongtincanhan.UserProfileUserId);
            return View(thongtincanhan);
        }

        //
        // POST: /ThongTinCaNhan/Edit/5

        [HttpPost]
        public ActionResult Edit(ThongTinCaNhan thongtincanhan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(thongtincanhan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", thongtincanhan.UserProfileUserId);
            return View(thongtincanhan);
        }

        //
        // GET: /ThongTinCaNhan/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ThongTinCaNhan thongtincanhan = db.ThongTinCaNhans.Find(id);
            if (thongtincanhan == null)
            {
                return HttpNotFound();
            }
            return View(thongtincanhan);
        }

        //
        // POST: /ThongTinCaNhan/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            ThongTinCaNhan thongtincanhan = db.ThongTinCaNhans.Find(id);
            db.ThongTinCaNhans.Remove(thongtincanhan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}