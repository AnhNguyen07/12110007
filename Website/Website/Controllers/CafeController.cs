﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models;

namespace Website.Controllers
{
    public class CafeController : Controller
    {
        private UsersContext db = new UsersContext();
        //
        // GET: /Cafe/

        public ActionResult Index()
        {
            return View();
        }
        //Tim kiem
        [AllowAnonymous]
        public ActionResult Search(string searchString)
        {
            var diadiems = from p in db.DiaDiems select p;
            if (!string.IsNullOrEmpty(searchString))
            {
                diadiems = diadiems.Where(y => y.Tags.Any(t => t.Content.Contains(searchString)));
            }
            return View(diadiems);
        }
        public ActionResult Cafe()
        {
            var ps = (from p in db.DiaDiems
                      where p.ChuDeCon == "Cafe"
                      orderby p.NgayTao descending
                      select p).Take(5).ToList();
            return View(ps);
        }
        public ActionResult Kem()
        {
            var ps = (from p in db.DiaDiems
                      where p.ChuDeCon == "Kem"
                      orderby p.NgayTao descending
                      select p).Take(5).ToList();
            return View(ps);
        }
        public ActionResult PhongTra()
        {
            var ps = (from p in db.DiaDiems
                      where p.ChuDeCon == "Phòng trà"
                      orderby p.NgayTao descending
                      select p).Take(5).ToList();
            return View(ps);
        }
        public ActionResult ChiTietDiaDiem(int id=0)
        {
            DiaDiem diadiem = db.DiaDiems.Find(id);
            ViewData["idpost"] = id;
            if (diadiem == null)
            {
                return HttpNotFound();
            }
            else
            {
                return View(diadiem);
            }
        }

    }
}
