namespace Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan7 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DiaDiems", "MoTa", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DiaDiems", "MoTa", c => c.String(nullable: false, maxLength: 2000));
        }
    }
}
