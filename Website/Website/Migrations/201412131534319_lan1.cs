namespace Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.ThongTinCaNhans",
                c => new
                    {
                        UserProfileUserId = c.Int(nullable: false),
                        HoTen = c.String(nullable: false, maxLength: 50),
                        GioiTinh = c.String(),
                        NgaySinh = c.DateTime(nullable: false),
                        Email = c.String(nullable: false),
                        SDT = c.String(nullable: false, maxLength: 20),
                        SoBaiDang = c.Int(nullable: false),
                        LuotDanhGia = c.Int(nullable: false),
                        DoTinCay = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserProfileUserId)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId)
                .Index(t => t.UserProfileUserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.ThongTinCaNhans", new[] { "UserProfileUserId" });
            DropForeignKey("dbo.ThongTinCaNhans", "UserProfileUserId", "dbo.UserProfile");
            DropTable("dbo.ThongTinCaNhans");
            DropTable("dbo.UserProfile");
        }
    }
}
