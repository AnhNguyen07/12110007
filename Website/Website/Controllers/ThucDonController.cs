﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models;

namespace Website.Controllers
{
    public class ThucDonController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /ThucDon/

        public ActionResult Index()
        {
            var thucdons = db.ThucDons.Include(t => t.DiaDiem);
            return View(thucdons.ToList());
        }

        //
        // GET: /ThucDon/Details/5

        public ActionResult Details(int id = 0)
        {
            ThucDon thucdon = db.ThucDons.Find(id);
            if (thucdon == null)
            {
                return HttpNotFound();
            }
            return View(thucdon);
        }

        //
        // GET: /ThucDon/Create

        public ActionResult Create()
        {
            ViewBag.DiaDiemID = new SelectList(db.DiaDiems, "ID", "Ten");
            return View();
        }

        //
        // POST: /ThucDon/Create

        [HttpPost]
        public ActionResult Create(ThucDon thucdon,int idpost)
        {
            if (ModelState.IsValid)
            {
                thucdon.DiaDiemID = idpost;
                
                //return RedirectToAction("Details/" + idpost, "DiaDiem");
                db.ThucDons.Add(thucdon);
                db.SaveChanges();
                return PartialView("_ViewThucDon", thucdon);
                //return RedirectToAction("Index");
            }

            ViewBag.DiaDiemID = new SelectList(db.DiaDiems, "ID", "Ten", thucdon.DiaDiemID);
            return View(thucdon);
        }

        //
        // GET: /ThucDon/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ThucDon thucdon = db.ThucDons.Find(id);
            if (thucdon == null)
            {
                return HttpNotFound();
            }
            ViewBag.DiaDiemID = new SelectList(db.DiaDiems, "ID", "Ten", thucdon.DiaDiemID);
            return View(thucdon);
        }

        //
        // POST: /ThucDon/Edit/5

        [HttpPost]
        public ActionResult Edit(ThucDon thucdon)
        {
            if (ModelState.IsValid)
            {
                db.Entry(thucdon).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DiaDiemID = new SelectList(db.DiaDiems, "ID", "Ten", thucdon.DiaDiemID);
            return View(thucdon);
        }

        //
        // GET: /ThucDon/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ThucDon thucdon = db.ThucDons.Find(id);
            if (thucdon == null)
            {
                return HttpNotFound();
            }
            return View(thucdon);
        }

        //
        // POST: /ThucDon/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            ThucDon thucdon = db.ThucDons.Find(id);
            db.ThucDons.Remove(thucdon);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}