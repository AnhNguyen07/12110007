﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models;

namespace Website.Controllers
{
    [Authorize]
    public class DiaDiemController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /DiaDiem/
        public ActionResult Index()
        {
            int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).Where(y => y.UserName == User.Identity.Name).Single().UserId;
            var diadiems = db.DiaDiems.Where(d => d.UserProfileUserId==userid).Include(d => d.DanhGia);
            return View(diadiems.ToList());
        }    
        //
        // GET: /DiaDiem/Details/5
        public ActionResult Details(int id = 0)
        {
            DiaDiem diadiem = db.DiaDiems.Find(id);
            ViewData["idpost"] = id;
            if (diadiem == null)
            {
                return HttpNotFound();
            }
            return View(diadiem);
        }

        //
        // GET: /DiaDiem/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            ViewBag.ID = new SelectList(db.DanhGias, "ID", "ID");
            return View();
        }

        //
        // POST: /DiaDiem/Create

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(DiaDiem diadiem,string Content)
        {
            if (ModelState.IsValid)
            {
                diadiem.NgayTao = DateTime.Now;
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserId;
                diadiem.UserProfileUserId = userid;
                List<Tag> Tags = new List<Tag>();
                string[] TagContent = Content.Split(',');
                foreach (string item in TagContent)
                {
                    Tag TagExit = null;
                    var ListTag = db.Tags.Where(p => p.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        TagExit = ListTag.First();
                        TagExit.DiaDiems.Add(diadiem);
                    }
                    else
                    {
                        TagExit = new Tag();
                        TagExit.Content = item;
                        TagExit.DiaDiems = new List<DiaDiem>();
                        TagExit.DiaDiems.Add(diadiem);
                    }
                    Tags.Add(TagExit);
                }
                diadiem.Tags = Tags;
                db.DiaDiems.Add(diadiem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", diadiem.UserProfileUserId);
            ViewBag.ID = new SelectList(db.DanhGias, "ID", "ID", diadiem.ID);
            return View(diadiem);
        }

        //
        // GET: /DiaDiem/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DiaDiem diadiem = db.DiaDiems.Find(id);
            if (diadiem == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", diadiem.UserProfileUserId);
            ViewBag.ID = new SelectList(db.DanhGias, "ID", "ID", diadiem.ID);
            return View(diadiem);
        }

        //
        // POST: /DiaDiem/Edit/5

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(DiaDiem diadiem)
        {
            if (ModelState.IsValid)
            {
                diadiem.NgayTao = DateTime.Now;
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserId;
                diadiem.UserProfileUserId = userid;
                db.Entry(diadiem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", diadiem.UserProfileUserId);
            ViewBag.ID = new SelectList(db.DanhGias, "ID", "ID", diadiem.ID);
            return View(diadiem);
        }

        //
        // GET: /DiaDiem/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DiaDiem diadiem = db.DiaDiems.Find(id);
            if (diadiem == null)
            {
                return HttpNotFound();
            }
            return View(diadiem);
        }

        //
        // POST: /DiaDiem/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            DiaDiem diadiem = db.DiaDiems.Find(id);
            db.DiaDiems.Remove(diadiem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}