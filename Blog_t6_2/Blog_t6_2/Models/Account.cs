﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_t6_2.Models
{
    public class Account
    {
        public int AccountID { set; get; }
        [Required(ErrorMessage = "Nhập mật khẩu!")]
        [StringLength(20, ErrorMessage = "Tối đa 20 kí tự!")]
        public string AccountName { set; get; }
        [Required(ErrorMessage="Nhập mật khẩu!")]
        [DataType(DataType.Password)]
        public string Password { set; get; }
        [Required(ErrorMessage = "Nhập địa chỉ email!")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Địa chỉ email không đúng!")]
        public string Email { set; get; }
        [Required(ErrorMessage="Nhập họ!")]
        [StringLength(100,ErrorMessage="Tối đa 100 kí tự!")]
        public string FirstName{set;get;}
        [Required(ErrorMessage = "Nhập Tên!")]
        [StringLength(100, ErrorMessage = "Tối đa 100 kí tự!")]
        public string LastName { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}