﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models;

namespace Website.Controllers
{
    public class TrangChuController : Controller
    {
        private UsersContext db = new UsersContext();
        //
        // GET: /TrangChu/

        public ActionResult Index()
        {
            ViewBag.View = "";
            return View();
        }
        //Tim kiem
        [AllowAnonymous]
        public ActionResult Search(string searchString)
        {
            var baiviets = from p in db.BaiViets select p;
            if (!string.IsNullOrEmpty(searchString))
            {
                baiviets = baiviets.Where(y => y.Tags.Any(t => t.Content.Contains(searchString)));
            }
            return View(baiviets);
        }
            public ActionResult PhongSuAmThuc()
        {
            var ps = (from p in db.BaiViets 
                     where p.ChuDeCon=="Phóng sự ẩm thực"
                     orderby p.NgayTao descending
                     select p).Take(5).ToList();
                return View(ps);
        }
            public ActionResult MonNgonMoiNgay()
            {
                var ps = (from p in db.BaiViets
                          where p.ChuDeCon == "Món ngon mỗi ngày"
                          orderby p.NgayTao descending
                          select p).Take(5).ToList();
                return View(ps);
            }
            public ActionResult SucKhoe()
            {
                var ps = (from p in db.BaiViets
                          where p.ChuDeCon == "Sức khoẻ"
                          orderby p.NgayTao descending
                          select p).Take(5).ToList();
                return View(ps);
            }
                public ActionResult ChiTiet(int id=0)
            {
                BaiViet baiviet = db.BaiViets.Find(id);
                ViewData["idpostbv"] = id;
                if (baiviet == null)
                {
                    return HttpNotFound();
                }
                else
                {
                    return View(baiviet);
                }
            }
               

    }
}
