// <auto-generated />
namespace Blog_T6_3.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class lan1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(lan1));
        
        string IMigrationMetadata.Id
        {
            get { return "201411200941080_lan1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
