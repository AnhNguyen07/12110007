﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models;

namespace Website.Controllers
{
    [Authorize(Users="admin")]
    public class BaiVietController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /BaiViet/

        public ActionResult Index()
        {
            return View(db.BaiViets.ToList());
        }
        //
        // GET: /BaiViet/Details/5

        public ActionResult Details(int id = 0)
        {
            BaiViet baiviet = db.BaiViets.Find(id);
            ViewData["idpostbv"] = id;
            if (baiviet == null)
            {
                return HttpNotFound();
            }
            return View(baiviet);
        }

        //
        // GET: /BaiViet/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /BaiViet/Create

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(BaiViet baiviet,string Content)
        {
            if (ModelState.IsValid)
            {
                baiviet.NgayTao = DateTime.Now;
                List<Tag> Tags = new List<Tag>();
                string[] TagContent = Content.Split(',');
                foreach (string item in TagContent)
                {
                    Tag TagExit = null;
                    var ListTag = db.Tags.Where(p => p.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        TagExit = ListTag.First();
                        TagExit.BaiViets.Add(baiviet);
                    }
                    else
                    {
                        TagExit = new Tag();
                        TagExit.Content = item;
                        TagExit.BaiViets = new List<BaiViet>();
                        TagExit.BaiViets.Add(baiviet);
                    }
                    Tags.Add(TagExit);
                }
                baiviet.Tags = Tags;
                db.BaiViets.Add(baiviet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(baiviet);
        }

        //
        // GET: /BaiViet/Edit/5
        public ActionResult Edit(int id = 0)
        {
            BaiViet baiviet = db.BaiViets.Find(id);
            if (baiviet == null)
            {
                return HttpNotFound();
            }
            return View(baiviet);
        }

        //
        // POST: /BaiViet/Edit/5

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(BaiViet baiviet)
        {
            if (ModelState.IsValid)
            {
                baiviet.NgayTao = DateTime.Now;
                db.Entry(baiviet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(baiviet);
        }

        //
        // GET: /BaiViet/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BaiViet baiviet = db.BaiViets.Find(id);
            if (baiviet == null)
            {
                return HttpNotFound();
            }
            return View(baiviet);
        }

        //
        // POST: /BaiViet/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            BaiViet baiviet = db.BaiViets.Find(id);
            db.BaiViets.Remove(baiviet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}