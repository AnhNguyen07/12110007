﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class ThucDon
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Tên món")]
        [StringLength(30, ErrorMessage = "Tối đa 30 kí tự!")]
        [Display(Name = "Tên món")]
        public string TenMon { set; get; }
        [Required(ErrorMessage = "Giá")]
        [Display(Name = "Giá")]
        public double Gia { set; get; }
        public int DiaDiemID { set; get; }
        public virtual DiaDiem DiaDiem { set; get; }
    }
}