﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class BinhLuanBaiViet
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Nhập nội dung!")]
        [StringLength(2000, ErrorMessage = "Nội dung tối đa 2000 kí tự!")]
        public string NoiDung { set; get; }
        [Display(Name = "Ngày tạo")]
        [DataType(DataType.DateTime)]
        public DateTime NgayTao { set; get; }
        [Display(Name = "Tên")]
        public string NguoiBinhLuan { set; get; }
        public int LanCuoi
        {
            get
            {
                return (DateTime.Now - NgayTao).Days * 60 * 24 + (DateTime.Now - NgayTao).Hours * 60 + (DateTime.Now - NgayTao).Minutes;
            }
        }
        public int BaiVietID { set; get; }
        public virtual BaiViet BaiViet { set; get; }
    }
}