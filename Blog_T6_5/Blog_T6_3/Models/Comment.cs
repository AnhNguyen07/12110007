﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_T6_3.Models
{
    public class Comment
    {
        [Key]
        public int ID { set; get; }
        [Required(ErrorMessage = "Nhập nội dung!")]
        [StringLength(2000, ErrorMessage = "Nội dung tối đa 2000 kí tự!")]
        public string Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreate { set; get; }
        public string Author { set; get; }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreate).Days * 60*24 + (DateTime.Now - DateCreate).Hours * 60 +(DateTime.Now - DateCreate).Minutes;
            }
        }
    }
}