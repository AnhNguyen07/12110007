﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models;

namespace Website.Controllers
{
    public class YeuThichNhatController : Controller
    {
        private UsersContext db = new UsersContext();
        //
        // GET: /Cafe/

        public ActionResult Index()
        {
            return View();
        }
        //Tim kiem
        [AllowAnonymous]
        public ActionResult Search(string searchString)
        {
            var diadiems = from p in db.DiaDiems select p;
            if (!string.IsNullOrEmpty(searchString))
            {
                diadiems = diadiems.Where(y => y.Tags.Any(t => t.Content.Contains(searchString)));
            }
            return View(diadiems);
        }
        public ActionResult MonAn()
        {
            var ps = (from p in db.DiaDiems
                      where p.ChuDeCon == "Món ăn"
                      orderby p.NgayTao descending
                      select p).Take(5).ToList();
            return View(ps);
        }
        public ActionResult DiaDiem()
        {
            var ps = (from p in db.DiaDiems
                      where p.ChuDeCon == "Địa điểm"
                      orderby p.NgayTao descending
                      select p).Take(5).ToList();
            return View(ps);
        }
        public ActionResult GiaRe()
        {
            var ps = (from p in db.DiaDiems
                      where p.ChuDeCon == "Giá rẻ"
                      orderby p.NgayTao descending
                      select p).Take(5).ToList();
            return View(ps);
        }
        public ActionResult ChiTietDiaDiem(int id = 0)
        {
            DiaDiem diadiem = db.DiaDiems.Find(id);
            ViewData["idpost"] = id;
            if (diadiem == null)
            {
                return HttpNotFound();
            }
            else
            {
                return View(diadiem);
            }
        }
    }
}
