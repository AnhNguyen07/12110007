﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_T6_3.Models;

namespace Blog_T6_3.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Post/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var posts = db.Posts.Include(p => p.UserProfile);
            return View(posts.ToList());
        }
        //Search
        [AllowAnonymous]
        public ActionResult Search(string searchString)
        {
            var posts = from p in db.Posts select p;
            if (!string.IsNullOrEmpty(searchString))
            {
                posts = posts.Where(y => y.Tags.Any(t => t.Content.Contains(searchString)));
            }
            return View(posts);
        }
        //
        // GET: /Post/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            ViewData["idpost"] = id;
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        public ActionResult Create(Post post,string Content)
        {
            if (ModelState.IsValid)
            {
                post.DateCreate = DateTime.Now;
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).Where(y => y.UserName == User.Identity.Name).Single().UserId;
                post.UserProfileUserId = userid;

                //List tag
                List<Tag> Tags = new List<Tag>();
                string[] TagContent = Content.Split(',');
                foreach (string item in TagContent)
                {
                    Tag TagExit = null;
                    var ListTag = db.Tags.Where(p => p.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        TagExit = ListTag.First();
                        TagExit.Posts.Add(post);
                    }
                    else
                    {
                        TagExit = new Tag();
                        TagExit.Content = item;
                        TagExit.Posts = new List<Post>();
                        TagExit.Posts.Add(post);
                    }
                    Tags.Add(TagExit);
                }
                post.Tags = Tags;
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}