﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_T6_3.Models
{
    public class Post
    {
        [Key]
        public int ID { set; get; }
        [Required(ErrorMessage = "Nhập tiêu đề!")]
        [StringLength(500, ErrorMessage = "Tiêu đề từ 20-500 kí tự!", MinimumLength = 20)]
        public string Title { set; get; }
        [Required(ErrorMessage = "Nhập nội dung!")]
        [StringLength(2000, ErrorMessage = "Nội dung tối thiểu có 50 kí tự!", MinimumLength = 50)]
        public string Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreate { set; get; }
        public int UserProfileUserId { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
    }
}