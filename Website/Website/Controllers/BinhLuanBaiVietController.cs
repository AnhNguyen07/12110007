﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models;

namespace Website.Controllers
{
    public class BinhLuanBaiVietController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /BinhLuanBaiViet/

        public ActionResult Index()
        {
            var binhluanbaiviets = db.BinhLuanBaiViets.Include(b => b.BaiViet);
            return View(binhluanbaiviets.ToList());
        }

        //
        // GET: /BinhLuanBaiViet/Details/5

        public ActionResult Details(int id = 0)
        {
            BinhLuanBaiViet binhluanbaiviet = db.BinhLuanBaiViets.Find(id);
            if (binhluanbaiviet == null)
            {
                return HttpNotFound();
            }
            return View(binhluanbaiviet);
        }

        //
        // GET: /BinhLuanBaiViet/Create

        public ActionResult Create()
        {
            ViewBag.BaiVietID = new SelectList(db.BaiViets, "ID", "TenBaiViet");
            return View();
        }

        //
        // POST: /BinhLuanBaiViet/Create

        [HttpPost]
        public ActionResult Create(BinhLuanBaiViet binhluanbaiviet,int idpostbv)
        {
            if (ModelState.IsValid)
            {
                binhluanbaiviet.BaiVietID = idpostbv;
                binhluanbaiviet.NgayTao = DateTime.Now;
                db.BinhLuanBaiViets.Add(binhluanbaiviet);
                db.SaveChanges();
                return PartialView("_ViewBinhLuanBaiViet",binhluanbaiviet);
                //return RedirectToAction("ChiTietPhongSu/" + idpostbv, "TrangChu");
            }

            ViewBag.BaiVietID = new SelectList(db.BaiViets, "ID", "TenBaiViet", binhluanbaiviet.BaiVietID);
            return View(binhluanbaiviet);
        }

        //
        // GET: /BinhLuanBaiViet/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BinhLuanBaiViet binhluanbaiviet = db.BinhLuanBaiViets.Find(id);
            if (binhluanbaiviet == null)
            {
                return HttpNotFound();
            }
            ViewBag.BaiVietID = new SelectList(db.BaiViets, "ID", "TenBaiViet", binhluanbaiviet.BaiVietID);
            return View(binhluanbaiviet);
        }

        //
        // POST: /BinhLuanBaiViet/Edit/5

        [HttpPost]
        public ActionResult Edit(BinhLuanBaiViet binhluanbaiviet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(binhluanbaiviet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BaiVietID = new SelectList(db.BaiViets, "ID", "TenBaiViet", binhluanbaiviet.BaiVietID);
            return View(binhluanbaiviet);
        }

        //
        // GET: /BinhLuanBaiViet/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BinhLuanBaiViet binhluanbaiviet = db.BinhLuanBaiViets.Find(id);
            if (binhluanbaiviet == null)
            {
                return HttpNotFound();
            }
            return View(binhluanbaiviet);
        }

        //
        // POST: /BinhLuanBaiViet/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            BinhLuanBaiViet binhluanbaiviet = db.BinhLuanBaiViets.Find(id);
            db.BinhLuanBaiViets.Remove(binhluanbaiviet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}