namespace Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BaiViets", "HinhAnh", c => c.String(nullable: false));
            AddColumn("dbo.DiaDiems", "HinhAnh", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DiaDiems", "HinhAnh");
            DropColumn("dbo.BaiViets", "HinhAnh");
        }
    }
}
