﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class DanhGia
    {
        [Key]
        [ForeignKey("DiaDiem")]
        public int ID { set; get; }
        public float GiaCa { set; get; }
        public float ChatLuong { set; get; }
        public float PhucVu { set; get; }
        public float ViTri { set; get; }
        public virtual DiaDiem DiaDiem { set; get; }
    }
}