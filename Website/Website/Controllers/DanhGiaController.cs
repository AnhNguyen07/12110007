﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models;

namespace Website.Controllers
{
    public class DanhGiaController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /DanhGia/

        public ActionResult Index()
        {
            var danhgias = db.DanhGias.Include(d => d.DiaDiem);
            return View(danhgias.ToList());
        }

        //
        // GET: /DanhGia/Details/5

        public ActionResult Details(int id = 0)
        {
            DanhGia danhgia = db.DanhGias.Find(id);
            if (danhgia == null)
            {
                return HttpNotFound();
            }
            return View(danhgia);
        }

        //
        // GET: /DanhGia/Create

        public ActionResult Create()
        {
            ViewBag.ID = new SelectList(db.DiaDiems, "ID", "Ten");
            return View();
        }

        //
        // POST: /DanhGia/Create

        [HttpPost]
        public ActionResult Create(DanhGia danhgia)
        {
            if (ModelState.IsValid)
            {
                db.DanhGias.Add(danhgia);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID = new SelectList(db.DiaDiems, "ID", "Ten", danhgia.ID);
            return View(danhgia);
        }

        //
        // GET: /DanhGia/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DanhGia danhgia = db.DanhGias.Find(id);
            if (danhgia == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID = new SelectList(db.DiaDiems, "ID", "Ten", danhgia.ID);
            return View(danhgia);
        }

        //
        // POST: /DanhGia/Edit/5

        [HttpPost]
        public ActionResult Edit(DanhGia danhgia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(danhgia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID = new SelectList(db.DiaDiems, "ID", "Ten", danhgia.ID);
            return View(danhgia);
        }

        //
        // GET: /DanhGia/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DanhGia danhgia = db.DanhGias.Find(id);
            if (danhgia == null)
            {
                return HttpNotFound();
            }
            return View(danhgia);
        }

        //
        // POST: /DanhGia/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            DanhGia danhgia = db.DanhGias.Find(id);
            db.DanhGias.Remove(danhgia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}