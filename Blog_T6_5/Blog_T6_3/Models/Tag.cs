﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_T6_3.Models
{
    public class Tag
    {
        [Key]
        public int ID { set; get; }
        [Required(ErrorMessage = "Nhập nội dung!")]
        [StringLength(100, ErrorMessage = "Nội dung từ 10 đén 50 kí tự!")]
        public string Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}