﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class BaiViet
    {
        [Key]
        public int ID { set; get; }
        [Display(Name = "Tiêu đề bài viết")]
        [Required(ErrorMessage = "Nhập tên địa điểm!")]
        [StringLength(50, ErrorMessage = "Nội dung tối thiểu 10 kí tự!")]
        public string TenBaiViet { set; get; }
        [Display(Name = "Chủ đề")]
        public string ChuDe { set; get; }
        [Display(Name = "Chủ đề con")]
        public string ChuDeCon { set; get; }
        [Display(Name = "Nội dung")]
        [Required(ErrorMessage = "Nhập nội dung!")]
        public string NoiDung { set; get; }
        [Display(Name = "Ngày Tạo")]
        [DataType(DataType.Date)]
        public DateTime NgayTao { set; get; }
        [Display(Name = "Tác giả")]
        public string TacGia { set; get; }
        [Display(Name = "Link ảnh")]
        [Required(ErrorMessage = "Nhập link ảnh")]
        public string HinhAnh { set; get; }
        public virtual ICollection<BinhLuanBaiViet> BinhLuanBaiViets { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}