namespace Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BaiViets",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenBaiViet = c.String(nullable: false, maxLength: 50),
                        ChuDe = c.String(),
                        ChuDeCon = c.String(),
                        NoiDung = c.String(nullable: false),
                        NgayTao = c.DateTime(nullable: false),
                        TacGia = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.BinhLuanBaiViets",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NoiDung = c.String(nullable: false, maxLength: 2000),
                        NgayTao = c.DateTime(nullable: false),
                        NguoiBinhLuan = c.String(),
                        BaiVietID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BaiViets", t => t.BaiVietID, cascadeDelete: true)
                .Index(t => t.BaiVietID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DiaDiems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Ten = c.String(nullable: false, maxLength: 50),
                        ChuDe = c.String(),
                        ChuDeCon = c.String(),
                        MoTa = c.String(nullable: false, maxLength: 2000),
                        DiaChi = c.String(nullable: false, maxLength: 500),
                        KhuVuc = c.String(),
                        SDT = c.String(nullable: false, maxLength: 20),
                        NgayTao = c.DateTime(nullable: false),
                        LuotThich = c.Int(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.BinhLuans",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NoiDung = c.String(nullable: false, maxLength: 2000),
                        NgayTao = c.DateTime(nullable: false),
                        NguoiBinhLuan = c.String(),
                        DiaDiemID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DiaDiems", t => t.DiaDiemID, cascadeDelete: true)
                .Index(t => t.DiaDiemID);
            
            CreateTable(
                "dbo.ThucDons",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenMon = c.String(nullable: false, maxLength: 30),
                        Gia = c.Double(nullable: false),
                        DiaDiemID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DiaDiems", t => t.DiaDiemID, cascadeDelete: true)
                .Index(t => t.DiaDiemID);
            
            CreateTable(
                "dbo.DanhGias",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        GiaCa = c.Single(nullable: false),
                        ChatLuong = c.Single(nullable: false),
                        PhucVu = c.Single(nullable: false),
                        ViTri = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DiaDiems", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.Tag_DiaDiem",
                c => new
                    {
                        DiaDiemID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DiaDiemID, t.TagID })
                .ForeignKey("dbo.DiaDiems", t => t.DiaDiemID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.DiaDiemID)
                .Index(t => t.TagID);
            
            CreateTable(
                "dbo.Tag_BaiViet",
                c => new
                    {
                        BaiVietID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.BaiVietID, t.TagID })
                .ForeignKey("dbo.BaiViets", t => t.BaiVietID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.BaiVietID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tag_BaiViet", new[] { "TagID" });
            DropIndex("dbo.Tag_BaiViet", new[] { "BaiVietID" });
            DropIndex("dbo.Tag_DiaDiem", new[] { "TagID" });
            DropIndex("dbo.Tag_DiaDiem", new[] { "DiaDiemID" });
            DropIndex("dbo.DanhGias", new[] { "ID" });
            DropIndex("dbo.ThucDons", new[] { "DiaDiemID" });
            DropIndex("dbo.BinhLuans", new[] { "DiaDiemID" });
            DropIndex("dbo.DiaDiems", new[] { "UserProfileUserId" });
            DropIndex("dbo.BinhLuanBaiViets", new[] { "BaiVietID" });
            DropForeignKey("dbo.Tag_BaiViet", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Tag_BaiViet", "BaiVietID", "dbo.BaiViets");
            DropForeignKey("dbo.Tag_DiaDiem", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Tag_DiaDiem", "DiaDiemID", "dbo.DiaDiems");
            DropForeignKey("dbo.DanhGias", "ID", "dbo.DiaDiems");
            DropForeignKey("dbo.ThucDons", "DiaDiemID", "dbo.DiaDiems");
            DropForeignKey("dbo.BinhLuans", "DiaDiemID", "dbo.DiaDiems");
            DropForeignKey("dbo.DiaDiems", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.BinhLuanBaiViets", "BaiVietID", "dbo.BaiViets");
            DropTable("dbo.Tag_BaiViet");
            DropTable("dbo.Tag_DiaDiem");
            DropTable("dbo.DanhGias");
            DropTable("dbo.ThucDons");
            DropTable("dbo.BinhLuans");
            DropTable("dbo.DiaDiems");
            DropTable("dbo.Tags");
            DropTable("dbo.BinhLuanBaiViets");
            DropTable("dbo.BaiViets");
        }
    }
}
