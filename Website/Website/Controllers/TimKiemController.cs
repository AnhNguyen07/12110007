﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models;

namespace Website.Controllers
{
    public class TimKiemController : Controller
    {
        private UsersContext db = new UsersContext();
        //
        // GET: /TimKiem/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Search(string String)
        {
            var news = (from p in db.DiaDiems.OrderByDescending(x=>x.NgayTao) select p);
            news = news.Where(y => y.DiaChi.Contains(@String)||
                y.Ten.Contains(@String)||
                y.ChuDe.Contains(@String)||
                y.ChuDeCon.Contains(@String)||
                y.ThucDons.Any(t => t.TenMon.Contains(@String)));
            return View(news);
        }
    }
}
