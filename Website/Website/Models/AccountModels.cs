﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace Website.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<BaiViet> BaiViets { set; get; }
        public DbSet<DanhGia> DanhGias { set; get; }
        public DbSet<ThucDon> ThucDons { set; get; }
        public DbSet<DiaDiem> DiaDiems { get; set; }
        public DbSet<BinhLuan> BinhLuans { get; set; }
        public DbSet<BinhLuanBaiViet> BinhLuanBaiViets { set; get; }
        public DbSet<Tag> Tags { set; get; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<DiaDiem>()
                .HasMany(d => d.Tags).WithMany(p => p.DiaDiems)
                .Map(t => t.MapLeftKey("DiaDiemID").MapRightKey("TagID").ToTable("Tag_DiaDiem"));
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<BaiViet>()
                .HasMany(d => d.Tags).WithMany(p => p.BaiViets)
                .Map(t => t.MapLeftKey("BaiVietID").MapRightKey("TagID").ToTable("Tag_BaiViet"));
            modelBuilder.Entity<ThongTinCaNhan>()
                .HasKey(t => t.UserProfileUserId);
            modelBuilder.Entity<UserProfile>()
                .HasRequired(t => t.ThongTinCaNhan)
                .WithRequiredPrincipal(t => t.UserProfile);
        }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<ThongTinCaNhan> ThongTinCaNhans { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        [Display(Name="Đăng bởi")]
        public string UserName { get; set; }
        public virtual ThongTinCaNhan ThongTinCaNhan { get; set; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
