﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class Tag
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Nhập nội dung!")]
        public string Content { set; get; }
        public virtual ICollection<DiaDiem> DiaDiems { set; get; }
        public virtual ICollection<BaiViet> BaiViets { set; get; }
    }
}