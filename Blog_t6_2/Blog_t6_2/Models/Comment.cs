﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_t6_2.Models
{
    public class Comment
    {
        public int ID { set; get; }
        [Required(ErrorMessage="Nhập nội dung!")]
        [StringLength(2000,ErrorMessage="Nội dung tối thiểu 50 kí tự!",MinimumLength=50)]
        public string Body { set; get; }
        [Required(ErrorMessage = "Nhập ngày giờ!")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreate { set; get; }
        [Required(ErrorMessage = "Nhập ngày giờ!")]
        [DataType(DataType.DateTime)]
        public DateTime DateUpdate { set; get; }
        [Required(ErrorMessage = "Nhập tên người comment!")]
        public string Author { set; get; }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}