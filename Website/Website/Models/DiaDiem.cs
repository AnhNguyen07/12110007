﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class DiaDiem
    {
        [Key]
        public int ID { set; get; }
        [Display(Name = "Tên địa điểm")]
        [Required(ErrorMessage = "Nhập tên địa điểm!")]
        [StringLength(50, ErrorMessage = "Nội dung tối thiểu 10 kí tự!")]
        public string Ten { set; get; }
        [Display(Name = "Chủ đề")]
        public string ChuDe { set; get; }
        [Display(Name = "Chủ đề con")]
        public string ChuDeCon { set; get; }
        [Display(Name = "Mô tả")]
        [Required(ErrorMessage = "Nhập nội dung!")]
        public string MoTa { set; get; }
        [Display(Name = "Địa chỉ")]
        [Required(ErrorMessage = "Nhập địa chỉ!")]
        [StringLength(500, ErrorMessage = "Nội dung tối đa 500 kí tự!")]
        public string DiaChi { set; get; }
        [Display(Name = "Khu vực")]
        public string KhuVuc { set; get; }
        [Display(Name = "Số điện thoại")]
        [Required(ErrorMessage = "Nhập số điện thoại!")]
        [StringLength(20, ErrorMessage = "Tối đa thiểu 10-20 kí tự!", MinimumLength = 10)]
        public string SDT { set; get; }
        [Display(Name = "Ngày tạo")]
        [DataType(DataType.DateTime)]
        public DateTime NgayTao { set; get; }
        [Display(Name = "Lượt thích")]
        public int LuotThich { set; get; }
        [Display(Name = "Link ảnh")]
        [Required(ErrorMessage = "Nhập link ảnh")]
        public string HinhAnh { set; get; }
        [Display(Name="Người đăng")]
        public int UserProfileUserId { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public virtual ICollection<BinhLuan> BinhLuans { set; get; }
        public virtual ICollection<ThucDon> ThucDons { set; get; }
        public virtual DanhGia DanhGia { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}